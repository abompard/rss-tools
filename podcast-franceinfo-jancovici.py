#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:
u"""
Créé un flux RSS avec les interventions de Jean-Marc Jancovici sur France
Info.

Les fichiers sonores sont placés dans des balises ``<enclosure/>`` pour faciliter le podcasting.

:Authors:
    Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

:License:
    GNU GPL v3 or later
"""

import os
import sys
import urllib2
import urlparse
import re
import BeautifulSoup
from pprint import pprint

list_url = "http://www.france-info.com/rss/Le_regard_de_Jean-Marc_Jancovici.xml"
base_url = "http://www.france-info.com"

js_re = re.compile("jstoflash\('play','.*','','','([^']*\.mp3)'\);")

list_page = urllib2.urlopen(list_url).read()
list_content = BeautifulSoup.BeautifulStoneSoup(list_page)
for item in list_content.find("atom:link").findAll(recursive=False):
    list_content.channel.insert(-1, item)
list_content.find("atom:link").extract()
for item in list_content.channel.findAll("item"):
    #print item.prettify()
    link = item.link.string
    #print link
    item_page = urllib2.urlopen(link).read()
    item_content = BeautifulSoup.BeautifulSoup(item_page)
    play_links = item_content.find("a", {"class": "p_ficheEcouter"})
    js_link = play_links["onclick"]
    js_mo = js_re.search(js_link)
    podcast_rel = js_mo.group(1)
    podcast_url = urlparse.urljoin(base_url, podcast_rel)
    #print "URL:", podcast_url
    item.enclosure["url"] = podcast_url
    try:
        podcast_info = urllib2.urlopen(podcast_url).info()
    except urllib2.URLError, e:
        sys.stderr.write("Error getting podcast info from %s: %s\n" %
                         (podcast_url, e))
        item.enclosure["type"] = "audio/mpeg" # reasonable fallback
        del item.enclosure["length"]
        continue
    item.enclosure["type"] = podcast_info.getheader("Content-Type")
    item.enclosure["length"] = podcast_info.getheader("Content-Length")

print list_content.prettify()
