#! /usr/bin/env python
# -*- coding: utf-8 -*-
u"""

TED talks subs
--------------

Download and convert the subtitles of a TED talk

Original script: http://estebanordano.com.ar/ted-talks-download-subtitles/

A few minor improvements by Aurélien Bompard <http://aurelien.bompard.org>

:License:
    MIT
"""

import optparse
import urllib
import sys
import re

import simplejson

def getFormatedTime(intvalue):
    mils = intvalue%1000
    segs = (intvalue/1000)%60
    mins = (intvalue/60000)%60
    hors = (intvalue/3600000)
    return "%02d:%02d:%02d,%03d"%(hors,mins,segs,mils)

def availableSubs(subs):
    a = subs.find("LanguageCode")
    if a == -1:
        return []
    subs = subs[a+len("LanguageCode"):]
    return [re.search("%22([^A-Z]+)%22", subs).group(1)] + availableSubs(subs)

def getVideoParameters(urldirection):
    ht = urllib.urlopen(urldirection).read()
    var = re.search('flashVars = {\n([^}]+)}', ht)
    if var:
        var = var.group(1)
    else:
        return None
    var = [a.replace('\t', '') for a in var.split('\n')]
    for a in range(len(var)):
        if var[a]:
            var[a] = var[a][:var[a].rfind(',')]
    resultado = []
    for a in var:
        l = a.find(':')
        if l != -1:
            resultado.append((a[:l], a[l+1:]))
    return dict(resultado)

def downloadSub(idtalk, lang, timeIntro):
    if options.debug:
        print("Downloading subtitles for language %s"%lang)
    c = simplejson.load(urllib.urlopen('http://www.ted.com/talks/subtitles/id/%s/lang/%s'%(idtalk, lang)))
    filename = options.output
    if not filename:
        filename = '%s_%s.srt' % (idtalk,lang)
    salida = open(filename, 'w')
    conta = 1
    c = c['captions']
    for linea in c:
        salida.write("%d\n"%conta)
        conta += 1
        salida.write("%s --> %s\n"%(getFormatedTime(timeIntro+linea['startTime']), getFormatedTime(timeIntro+linea['startTime']+linea['duration'])))
        salida.write("%s\n\n"%(linea['content'].encode('utf-8')))
    salida.close()

def main(idtalk):
    if options.debug:
        print("Loading information about TED talk number %s..."%idtalk)
    vidpar = getVideoParameters('http://www.ted.com/talks/view/id/%s'%idtalk)
    if not vidpar:
        print >>sys.stderr, "There was a problem fetching information about that TED Talk"
        sys.exit(1)
    availables = availableSubs(vidpar['languages'])
    if options.list:
        print "\n".join(availables)
        sys.exit(0)
    if options.lang and options.lang not in availables:
        print >>sys.stderr, "Sorry, this language is not available. " \
                           +"Available languages:"
        if not len(availables):
            print >>sys.stderr, "(none)"
        else:
            print >>sys.stderr, "\n".join(availables)
        sys.exit(1)
    if not options.lang:
        print("Download all subtitles (write 'all' when prompted) or only one (specify wich)?")
        lang = raw_input()
        while lang not in availables:
            print("We're sorry, the only available languages are:")
            print "\t".join(availables)
            lang = raw_input()
    else:
        lang = options.lang
    if lang == "all":
        for lang in availables:
            downloadSub(idtalk, lang, int(vidpar['introDuration']))
    else:
        downloadSub(idtalk, lang, int(vidpar['introDuration']))

def parse_opts():
    usage = "usage: %prog [-l lang] [-o output] [-t] talkid"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-l", "--lang", dest="lang",
                      help="Use this language")
    parser.add_option("-o", "--output", dest="output",
                      help="Write the subtitle in this file")
    parser.add_option("-t", "--list-only", dest="list", action="store_true",
                      help="List available subtitles and exit")
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      help="Print debug information")
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.error("You must give a talk ID")
    try:
        int(args[0])
    except ValueError:
        parser.error("The argument must be the numeric unique talk ID")
    return options, args[0]

if __name__ == "__main__":
    global options
    options, talkid = parse_opts()
    main(talkid)
