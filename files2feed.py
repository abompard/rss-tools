#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:

u"""

Files to Feed
-------------

Create an ATOM feed from the files in a directory

.. :Authors:
      Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
      GNU GPL v3 or later
"""

import os
import sys
import socket
import datetime
import hashlib
import cgi
import urllib
import optparse


def main(directory, options):
    feed = open(options.output, "w")

    feed.write("""<?xml version="1.0" encoding="utf-8" ?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>%(title)s</title>
    <link rel="self" href="%(address)s/%(feed_name)s" />
    <updated>%(date)s</updated>
    <id>%(address)s</id>
""" % { "feed_name": os.path.basename(options.output),
        "title": options.title,
        "address": options.url,
        "date": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")})

    files_raw = []
    for root, dirs, cur_files in os.walk(directory):
        for subdir in dirs:
            # Exclude dirs
            if subdir in options.exclude or subdir.startswith("."):
                del dirs[dirs.index(subdir)]
                if options.debug:
                    print "excluding", subdir
                continue
            # Recurse into symlinks to dirs (included in python >= 2.6)
            if os.path.islink(os.path.join(root,subdir)):
                for subroot, subsubdirs, subcur_files in \
                        os.walk(os.path.join(root,subdir)):
                    for subsubdir in subsubdirs:
                        # Exclude dirs
                        if subsubdir in options.exclude \
                                or subsubdir.startswith("."):
                            del subsubdirs[subsubdirs.index(subsubdir)]
                            if options.debug:
                                print "excluding", subsubdir
                            continue
                    for file in subcur_files:
                        files_raw.append(os.path.join(subroot,file))
        for file in cur_files:
            files_raw.append(os.path.join(root,file))

    files = []
    for file in files_raw:
        if os.path.basename(file) in options.exclude \
                or os.path.basename(file).startswith("."):
            continue
        if os.path.basename(file) == os.path.basename(sys.argv[0]):
            continue
        if os.path.basename(file) == os.path.basename(options.output):
            continue
        try:
            date = os.path.getmtime(file)
            size = os.path.getsize(file)
            files.append( (file.replace(directory,""), date, size) )
        except OSError:
            print "WARNING: can't access file %s" % file

    files.sort(cmp=lambda x,y: cmp(x[1],y[1]), reverse=True)

    for file in files[:options.max]:
        id = hashlib.md5()
        id.update(file[0])
        id = id.hexdigest()
        feed.write("""    <entry>
        <title>%(basename)s</title>
        <updated>%(mtime)s</updated>
        <link href=\"%(address)s%(filename)s\" />
        <summary type=\"html\">&lt;p&gt;%(basename)s (%(size)sKo)&lt;br/&gt;In: %(path)s&lt;/p&gt;</summary>
        <id>tag:%(host)s:%(md5)s</id>
    </entry>
""" % { "basename": cgi.escape(os.path.basename(file[0])), 
        "mtime": datetime.datetime.fromtimestamp(file[1]).strftime("%Y-%m-%dT%H:%M:%SZ"),
        "host": options.hostname,
        "filename": urllib.quote(file[0]),
        "path": cgi.escape(os.path.dirname(file[0])),
        "size": int(file[2]) / 1024,
        "year": datetime.date.today().year,
        "md5": id,
        "address": options.url,
      })

    feed.write("</feed>\n")
    feed.close()


def parse_opts():
    usage = "usage: %prog [options] [directory]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-o", "--output", dest="output", metavar="FILE",
                      default="files.xml",
                      help="write to this file (default: %default)")
    parser.add_option("-m", "--max", dest="max", default=50, type="int",
                      help="the maximum number of files to include in the "
                      "feed (default: %default)")
    parser.add_option("-t", "--title", dest="title", default="Files on %s",
          help="title for the feed. You can use %s to include the hostname.")
    parser.add_option("-x", "--exclude", dest="exclude", default="",
                      metavar="DIR1,DIR2,...", help="a comma-sparated list "
                      "of directories that should not be crawled")
    parser.add_option("-r", "--root", dest="root", help="the document root "
                      "(where the directory starts to be network-accessible")
    parser.add_option("--hostname", dest="hostname",
              default=socket.gethostname(),
              help="the hostname to make links point to (default: %default)")
    parser.add_option("--url-scheme", dest="url_scheme", default="http",
                      help="the URL method (default: %default)")
    parser.add_option("--url-port", dest="url_port",
                      help="the URL port, if not default for the chosen scheme")
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      help="Debug mode")
    opts, args = parser.parse_args()

    if len(args) == 0:
        directory = os.getcwd()
    elif len(args) == 1:
        directory = args[0]
    else:
        parser.error("Only one feed can be given as argument")
    if not os.path.isdir(directory):
        parser.error("The directory '%s' does not exist" % directory)
    directory = os.path.abspath(directory)

    if opts.title.count("%"):
        opts.title = opts.title % opts.hostname

    opts.exclude = [ d.strip() for d in opts.exclude.split(",") ]

    if not opts.root:
        parser.error("You must provide a document root")

    url = [opts.url_scheme, "://", opts.hostname]
    if opts.url_port:
        url.append(":", opts.url_port)
    path = directory.replace(opts.root, "")
    if not path.startswith("/"):
        url.append("/")
    url.append(path)
    url = "".join(url)
    opts.url = url

    return opts, directory


if __name__ == "__main__":
    opts, directory = parse_opts()
    main(directory, opts)
