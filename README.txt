RSS scripts
===========

This repository contains various scripts I made to generate RSS, Atom and
podcast feeds.

There should be a short description at the top of each script.

Feel free to use them as you want (under the terms of the license). You can
contact me about them at <aurelien@bompard.org> or on my website:
<http://aurelien.bompard.org>.

They are all licensed under the GNU GPL version 3 or later.
(c) copyright Aurélien Bompard 2009-2014

