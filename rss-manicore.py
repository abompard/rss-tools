#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:
u"""
Créé un flux RSS avec la page des actualités du site http://manicore.com

:Authors:
    Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

:License:
    GNU GPL v3 or later
"""

import os
import sys
import urllib2
import re
import datetime
import locale
from urlparse import urljoin
from optparse import OptionParser

import BeautifulSoup


BASE_URL = "http://manicore.com"
MAX_ITEMS = 10

TEXT_START_RE = re.compile("^\s*:\s*")

FEED_HEAD = u"""<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title>%(title)s</title>
        <description>%(desc)s</description>
        <link>%(base_url)s</link>
        <language>fr</language>
        <lastBuildDate>%(now)s GMT</lastBuildDate>
        <pubDate>%(now)s GMT</pubDate>
        <atom:link href="%(feed_url)s" rel="self" type="application/rss+xml" />
        <image>
            <title>%(title)s</title>
            <link>%(base_url)s</link>
            <url>%(logo)s</url>
        </image>
"""

FEED_ENTRY = u"""
        <item>
            <title>%(title)s</title>
            <description>
                <![CDATA[
                    %(desc)s
                ]]>
            </description>
            <link>%(link)s</link>
            <guid isPermaLink="false">%(guid)s</guid>
            <pubDate>%(date)s +0100</pubDate>
        </item>
"""

FEED_TAIL = u"""
    </channel>
</rss>
"""


class FeedItem(object):

    fixes = {
        "decembre": u"décembre",
    }
    date_re = re.compile("\s*(\d\d?)\s+(\S+)\s+(\d\d\d\d)\s*")
    feed_entry = FEED_ENTRY

    def __init__(self):
        self.vars = {
            "date": None,
            "desc": u"",
            "link": None,
            "title": u"",
        }

    def render(self):
        vars = self.vars.copy()
        if not self.vars["date"]:
            vars["date"] = ""
            vars["guid"] = ("urn:www:manicore.com:%s" % self.vars["link"])
        else:
            vars["date"] = self.vars["date"].strftime("%a, %d %b %Y %H:%M:%S")
            vars["guid"] = ("urn:www:manicore.com:%s:%s"
                            % (self.vars["date"].strftime("%Y-%m-%d"),
                               os.path.basename(self.vars["link"])))
        return unicode(self.feed_entry % vars)

    def __str__(self):
        return self.render()

    def check(self, item):
        return True

    def parse(self, item):
        for subitem in item:
            if isinstance(subitem, BeautifulSoup.NavigableString):
                self.vars["desc"] += unicode(subitem)
                self.vars["title"] += unicode(subitem)
            elif isinstance(subitem, BeautifulSoup.Tag):
                if subitem.name == "a":
                    subitem["href"] = urljoin(self.page_url, subitem["href"])
                if subitem.name == "font" or subitem.name == "p":
                    self.parse(subitem)
                elif subitem.name == "strong" or subitem.name == "b":
                    self.parse_date(subitem)
                elif not self.vars["link"] and subitem.name == "a":
                    self.parse_link(subitem)
                else:
                    self.add_to_desc(subitem)

    def add_to_desc(self, item):
        self.vars["desc"] += unicode(item)
        if item.string:
            self.vars["title"] += item.string
    
    def parse_date(self, item):
        if self.vars["date"]:
            self.add_to_desc(item)
            return
        if not item.string:
            #print >>sys.stderr, "Recursing for", item.prettify()
            for subitem in item:
                self.parse_date(subitem)
            return
        date = item.string.replace("1er","1")
        date_mo = self.date_re.match(date)
        if not date_mo:
            self.vars["desc"] += unicode(item)
            return
        date = "%s %s %s" % (date_mo.group(1), date_mo.group(2), date_mo.group(3))
        try:
            self.vars["date"] = datetime.datetime.strptime(
                                    date.encode("utf-8"), "%d %B %Y")
        except ValueError:
            # Un autre essai en tentant de corriger les erreurs
            for fix in self.fixes:
                if item.string.count(fix):
                    item.string = item.string.replace(fix, self.fixes[fix])
                    return self.parse_date(item)
            raise

    def parse_link(self, item):
        self.vars["link"] = item["href"].replace("&","&amp;")
        self.add_to_desc(item)

    def cleanup(self):
        self.vars["desc"] = TEXT_START_RE.sub("", self.vars["desc"])
        self.vars["title"] = TEXT_START_RE.sub("", self.vars["title"])
        if len(self.vars["title"]) > 100:
            self.vars["title"] = self.vars["title"][:98] + "..."


class NewsItem(FeedItem):
    page_url = "http://manicore.com/actualites.html"
    page_item_attrs = {"align": "justify"}
    feed_attrs = {
        "title": "Manicore",
        "desc": u"Bienvenue chez Jean-Marc Jancovici, le seul consultant qui "
                u"vous offre la Lune sans que vous ayez à la demander...",
        "logo": "http://manicore.com/ressources/moon.JPG",
        "feed_url": "http://feeds.feedburner.com/manicore-actu",
    }

class FranceInfoItem(FeedItem):

    date_re = re.compile("[^\d]*(\d\d?)\s+(\S+)\s+(\d\d\d\d).*")
    page_url = "http://manicore.com/documentation/articles/France_info_chroniques.html"
    page_item_attrs = {"class": "normal"}
    feed_attrs = {
        "title": "Le Regard de Jean-Marc Jancovici",
        "desc": u"L'analyse de l'actualité de la semaine par Jean-Marc "
                u"Jancovici",
        "logo": "http://manicore.com/ressources/moon.JPG",
        "feed_url": "http://feeds.feedburner.com/franceinfo-jancovici",
    }
    feed_entry = u"""
        <item>
            <title>%(title)s</title>
            <link>%(link)s</link>
            <guid isPermaLink="false">%(guid)s</guid>
            <pubDate>%(date)s +0100</pubDate>
            <enclosure url="%(enclosure)s" length="%(length)d" type="audio/mpeg" />
        </item>
"""

    def __init__(self):
        super(FranceInfoItem, self).__init__()
        self.vars["enclosure"] = None
        self.vars["length"] = 0

    def check(self, item):
        return (item.font and item.font.img and item.a)

    def parse(self, item):
        self.parse_date(item)
        self.vars["title"] = item.a.string
        self.vars["link"] = urljoin(self.page_url, item.a["href"])
        podcast = item.find("a", href=lambda h: h.endswith(".mp3"))
        if podcast is not None:
            self.vars["enclosure"] = urljoin(self.page_url, podcast["href"])
            self.get_podcast_length()

    def parse_date(self, item):
        date = None
        for item_content in item.contents:
            if not isinstance(item_content, BeautifulSoup.NavigableString):
                continue
            match = self.date_re.match(item_content)
            if match is None:
                continue
            date = "%s %s %s" % (match.group(1), match.group(2),
                                 match.group(3))
        if not date:
            return
        try:
            self.vars["date"] = datetime.datetime.strptime(
                                    date.encode("utf-8"), "%d %B %Y")
        except ValueError:
            # Un autre essai en tentant de corriger les erreurs
            for fix in self.fixes:
                if item.string.count(fix):
                    item.string = item.string.replace(fix, self.fixes[fix])
                    return self.parse_date(item)
            raise

    def get_podcast_length(self):
        try:
            podcast_info = urllib2.urlopen(self.vars["enclosure"]).info()
        except urllib2.URLError, e:
            sys.stderr.write("Error getting podcast info from %s: %s\n" %
                             (self.vars["enclosure"], e))
            return
        self.vars["length"] = int(podcast_info.getheader("Content-Length"))


PAGES = {
    "actualites": NewsItem,
    "franceinfo": FranceInfoItem,
}


def get_items(page_class):
    page_text = urllib2.urlopen(page_class.page_url).read()
    page_soup = BeautifulSoup.BeautifulSoup(page_text,
                                            convertEntities="html")
    locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
    page_content = page_soup.find("div", id="contenu")
    items = []
    for page_item in page_content.findAll("p", limit=MAX_ITEMS,
                                     attrs=page_class.page_item_attrs):
        #print item
        item = page_class()
        if not item.check(page_item):
            continue
        item.parse(page_item)
        item.cleanup()
        #print "date:", news.date, datetime.date.today().strftime("%d %B %Y")
        #print "link:", news.link
        #print "title:", news.title
        #print "text:", news.text
        items.append(item)

    locale.setlocale(locale.LC_ALL, '')
    return items

def build_feed(page_class, items):
    feed_info = page_class.feed_attrs.copy()
    feed_info["base_url"] = BASE_URL
    feed_info["now"] = datetime.datetime.utcnow().strftime(
                                "%a, %d %b %Y %H:%M:%S")
    feed = FEED_HEAD % feed_info

    for item in items:
        if not item.vars["date"]:
            print >> sys.stderr, u"Could not parse date for item: %s" % unicode(item)
            continue
        feed += unicode(item.render())

    feed += FEED_TAIL

    return feed.encode("utf-8")

def parse_opts():
    parser = OptionParser()
    parser.add_option("-o", "--output", help="Write the feed to this file")
    opts, args = parser.parse_args()
    if len(args) != 1:
        message = ("You must provide the page name as argument. "
                   "Available pages: %s" % ", ".join(PAGES.keys()))
        parser.error(message)
    return opts, args[0]

def main():
    opts, page_name = parse_opts()
    #list_page = open("actualites.html").read()
    page_class = PAGES[page_name]
    items = get_items(page_class)
    feed = build_feed(page_class, items)
    if opts.output is None:
        print feed
    else:
        out = open(opts.output, "w")
        out.write(feed)
        out.close()


if __name__ == "__main__":
    main()
