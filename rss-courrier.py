#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:

import os
import optparse
from urllib2 import urlopen
import xml.etree.ElementTree as etree

RSS_URL_ORIG = "http://cartoons.courrierinternational.com/rss/all/rss.xml"


def parse_opts():
    parser = optparse.OptionParser()
    parser.add_option("-o", "--output", help="Write to this file")
    parser.add_option("-v", "--verbose")
    opts = parser.parse_args()[0]
    if opts.output is None:
        parser.error("You must choose a file to write to with --output")
    return opts

def main():
    opts = parse_opts()
    rss_url = urlopen(RSS_URL_ORIG)
    rss = etree.parse(rss_url)
    rss_url.close()
    #rss = etree.parse("rss.xml")
    for enclosure in rss.findall("//enclosure"):
        thumb_url = enclosure.get("url")
        full_url = thumb_url.replace("/dessin_vignette/", "/dessin_a_la_une/")
        full_url = full_url[:-5] + full_url[-4:]
        enclosure.set("url", full_url)
        if opts.verbose:
            print thumb_url, " -> ", full_url
    rss.write(opts.output, "utf-8")


if __name__ == "__main__":
    main()
