#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""

TrocDesTrains to RSS
--------------------

The comments and the documentation is in French, because it's only useful to
french people.

Ce script convertit une recherche sur le site trocdestrains.com en un flux RSS.


.. :Authors:
       Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
       GNU GPL v3 or later

"""


import sys
import re
import datetime
import time
from optparse import OptionParser
from xml.sax.saxutils import escape
from random import randrange

import mechanize
from BeautifulSoup import BeautifulSoup



class Billet(object):

    url = None

    gare_dep = None
    gare_arr = None
    gares_interm = []

    heure_dep = None
    heure_arr = None
    heures_interm = []

    date = None
    train = None
    place = None
    prix = None

    pop_vendeur = None
    date_depot = None
    nb_contacts = None
    fiabilite = None
    prix_paye = None


    def __init__(self, tag):
        self.parse(tag)


    def parse(self, tag):
        self.url = tag["href"]

        gare_rech = tag.findAll("span", attrs={"class": "gare-rech"})
        self.gare_dep, self.gare_arr = [ g.string.strip() for g in gare_rech ]
        gares_interm = tag.findAll("span", attrs={"class": "gare-interm"})
        self.gares_interm = self._list_to_string(gares_interm)

        heure_rech = tag.findAll("span", attrs={"class": "heure-rech"})
        self.heure_dep, self.heure_arr = [ h.string.strip() for h in heure_rech ]
        heures_interm = tag.findAll("span", attrs={"class": "heure-interm"})
        self.heures_interm = self._list_to_string(heures_interm)

        self.date = tag.find("span", attrs={"class": "date"}).string.strip()
        self.train = tag.find("span", attrs={"class": "train"}).string.strip()
        self.place = ' '.join(t.string.strip() for t in
                             tag.findAll("span", attrs={"class": "train"})[1].contents)
        self.place = self.place.replace("  &egrave;", u"è").replace("  nde", "nde").strip()
        self.prix = tag.find("span", attrs={"class": "prix"}).contents[0].strip().replace("&euro;", u"€")

        #self.pop_vendeur = self._parse_fiabilite(
        #        tag.find("span", attrs={"class": "fiabilite"}))
        contacts = ''.join(t.string for t in
                           tag.find("span", attrs={"class": "contacts"}).contents)
        mo = re.match("\s*\(([0-9/]+)\)&nbsp;Contacts:\s*(\d+)", contacts)
        if mo:
            self.date_depot = mo.group(1)
            self.nb_contacts = int(mo.group(2))
        #self.fiabilite = self._parse_fiabilite(
        #        tag.findAll("span", attrs={"class": "fiabilite"})[1])
        self.prix_paye = tag.find("span", attrs={"class": "prix-paye"}).contents[2].strip().replace("&euro;", u"€")


    def _list_to_string(self, tags):
        result = []
        for tag in tags:
            string = tag.string.replace("&nbsp;", "").strip()
            if string:
                result.append(string)
        return result

    def _parse_fiabilite(self, tag):
        spantag = tag.findAll("span")[1]
        string = ''.join(t.string for t in spantag.findAll())
        return string.count("*")

    def pretty(self):
        result = u"%s -> %s le %s" % (self.gare_dep, self.gare_arr, self.date)
        if self.gares_interm:
            result += u" (via %s)" % ", ".join(self.gares_interm)
        result += u" pour %s.\n" % self.prix
        result += u"Départ à %s, arrivée à %s.\n" % (self.heure_dep, self.heure_arr)
        result += u"%s, %s. Contacts: %d" % (self.place, self.train, self.nb_contacts)
        return result

    def to_xml(self):
        depot = datetime.datetime.strptime(self.date_depot, "%d/%m")
        depot = depot.replace(year=datetime.date.today().year)
        result = (u"""
            <item>
                <title>%(date)s à %(heure_dep)s : %(prix)s</title>
                <description>
                    %(desc)s
                </description>
                <pubDate>%(pubdate)s</pubDate>
                <link>%(url)s</link>
                <guid isPermaLink="true">%(url)s</guid>
            </item>""" % {
                "date": self.date,
                "heure_dep": self.heure_dep,
                "prix": self.prix,
                "desc": escape(self.pretty()),
                "pubdate": depot.isoformat(),
                "url": escape(self.url),
              })
        return result



def make_request(options):
    ville_dep, ville_arr, date_dep, output = options
    br = mechanize.Browser()
    br.set_handle_robots(False)
    #br.set_debug_http(True)
    br.addheaders = [("User-agent", "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/11.0"), ]
    req = mechanize.Request("http://trocdestrains.com/")
    req.add_header("User-Agent", "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/11.0")
    resp = br.open(req)
    #print br, br.title()
    br.select_form("recherche_prems")
    br["L_jour_dep"] = [date_dep.strftime("%d")]
    br["L_mois_annee_dep"] = [date_dep.strftime("%Y-%m")]
    br["ville_dep"] = [ville_dep]
    br["ville_arr"] = [ville_arr]
    response = br.submit(name="choix_rech_billet")
    #with open("/tmp/response.html", "w") as log:
    #    log.write(response.read())
    return response


def parse_response(response):
    soup = BeautifulSoup(response.read())
    billet_tags = soup.find("div", attrs={"class": "liste-billets"}
                           ).findAll("a", attrs={"class": "bulle-billet"})
    billets = []
    for tag in billet_tags:
        try:
            billet = Billet(tag)
        except AttributeError:
            print tag.prettify()
            raise
        billets.append(billet)
    return billets


def write_rss(billets, options):
    ville_dep, ville_arr, date_dep, output = options
    rss_header = u"""<?xml version="1.0" encoding="utf-8"?>
    <rss version="2.0">
        <channel>
            <title>Troc des Trains : de %(dep)s à %(arr)s le %(date)s</title>
            <description>
                Billets disponibles sur Troc des Trains pour le %(date)s de %(dep)s à %(arr)s.
            </description>
            <lastBuildDate>%(now)s</lastBuildDate>
            <link>http://www.trocdestrains.com/</link>
    """ % {
        "dep": ville_dep,
        "arr": ville_arr,
        "date": date_dep.strftime("%d/%m"),
        "now": datetime.datetime.now().isoformat(),
    }
    rss_footer = "\n\n    </channel>\n</rss>\n"

    if isinstance(output, basestring):
        output = open(output, "w")
    output.write(rss_header.encode("utf-8"))
    for billet in billets:
        output.write(billet.to_xml().encode("utf-8"))
    output.write(rss_footer)
    output.close()


def parse_opts():
    parser = OptionParser()
    parser.add_option("-f", "--from", dest="ville_dep", help=u"ville de départ")
    parser.add_option("-t", "--to", dest="ville_arr", help=u"ville d'arrivée")
    parser.add_option("-d", "--day", help=u"jour du départ")
    parser.add_option("-m", "--month", help=u"mois du départ")
    parser.add_option("-y", "--year", help=u"année du départ")
    parser.add_option("-o", "--output", default=sys.stdout,
                      help=u"fichier de sortie")
    parser.add_option("--sleep", metavar="M",
                      help="attendre entre 0 et M minutes avant de commencer")
    #parser.add_option("--date", help=u"date du départ au format JJ-MM-AAAA")
    opts, args = parser.parse_args()
    if args:
        parser.error(u"pas d'arguments autorisés.")
    if not opts.ville_dep:
        parser.error(u"une ville de départ est nécessaire.")
    if not opts.ville_arr:
        parser.error(u"une ville d'arrivée est nécessaire.")
    date = datetime.date.today()
    if opts.day:
        date = date.replace(day=int(opts.day))
    if opts.month:
        date = date.replace(month=int(opts.month))
    if opts.year:
        date = date.replace(year=int(opts.year))
    if date < datetime.date.today():
        parser.error(u"il faut choisir une date dans le futur")
    if opts.sleep:
        sleeptime = randrange(int(opts.sleep) * 60)
        time.sleep(sleeptime)
    return (opts.ville_dep, opts.ville_arr, date, opts.output)


def main():
    options = parse_opts()
    response = make_request(options)
    #with open("/tmp/response.html") as response:
    #    soup = BeautifulSoup(response.read())
    billets = parse_response(response)
    write_rss(billets, options)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print >>sys.stderr, u"\rArrêt à la demande de l'utilisateur."
        sys.exit(1)
